// -------- INITIALIZE OBJECTS ------- //
let PhoDAO = new PhotographerDAO();
let Photographers = PhoDAO.myPhotographers();
PhoDAO.fillHomePage(Photographers);
PhoDAO.clickobserver(Photographers);

let MedDAO = null;
let FormuDAO = null;
let Medias = null;
let Modal = null;
// -------- .INITIALIZE OBJECTS ------- //

var path = window.location.pathname;
var page = path.split("/").pop();
let p = window.location.pathname;
if (p.length === 0 || p === "/" || page == "") {
    // -------- SCROLLUP ------- //
    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
        let currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            document.getElementById("scrollup").style.display = "none";
        } else {
            document.getElementById("scrollup").style.display = "block";
        }
        prevScrollpos = currentScrollPos;
    };
    // -------- .SCROLLUP ------- //
} else {
    // -------- IF NOT HOMEPAGE RUN THESES SCRIPTS ------- //
    MedDAO = new MediaDAO();
    FormuDAO = new FormDAO();
    ModDAO = new ModalDAO();
    Medias = MedDAO.myMedia();
    MedDAO.fillPhotographerPage(Photographers, Medias);
    // -------- .IF NOT HOMEPAGE RUN THESES SCRIPTS ------- //
}

setTimeout(function () {
    // -------- MODAL FORM ------- //

    //On click button sumbit form
    const submitBtn = document.querySelectorAll(".btn-submit");
    submitBtn.forEach((btn) => btn.addEventListener("click", FormuDAO.submit)); //button submit

    //On submit do event
    const submitForm = document.querySelectorAll("#contact");
    submitForm.forEach((btn) =>
        btn.addEventListener("submit", FormuDAO.submit)
    );

    //open modal
    const openmodal = document.querySelectorAll(".open_modal");
    openmodal.forEach((btn) =>
        btn.addEventListener("click", FormuDAO.launchModal)
    );

    //close modal
    const closemodal = document.querySelectorAll(".close_modal");
    closemodal.forEach((btn) =>
        btn.addEventListener("click", FormuDAO.closeModal)
    );
    // -------- .MODAL FORM ------- //

    // -------- FILTER SELECT------- //

    //Filter #something
    const clickfilter = document.querySelectorAll(".filter");
    clickfilter.forEach((btn) =>
        btn.addEventListener("click", PhoDAO.redirect_filter)
    );

    //Filter MEDIAS
    const onChangeFilter = document.querySelectorAll("#photo-select");
    onChangeFilter.forEach((btn) =>
        btn.addEventListener(
            "change",
            function () {
                MedDAO.sort(Photographers, Medias);
            },
            false
        )
    );
    // -------- .FILTER SELECT------- //

    // -------- LIGHTBOX MEDIAS ------- //

    //open lightbox
    const show_media = document.querySelectorAll(".show_media");
    show_media.forEach((btn) =>
        btn.addEventListener(
            "click",
            function () {
                ModDAO.launchModalMedia(this);
            },
            false
        )
    );

    //close lightbox
    const close_media = document.querySelectorAll(".close_modal_media");
    close_media.forEach((btn) =>
        btn.addEventListener("click", ModDAO.closeModalMedia)
    );

    if (page != "") {
        document
            .querySelector(".carousel-control.left")
            .addEventListener("click", function () {
                if (window.isEnabled) {
                    ModDAO.previousItem(window.currentItem);
                }
            });

        document
            .querySelector(".carousel-control.right")
            .addEventListener("click", function () {
                if (window.isEnabled) {
                    ModDAO.nextItem(window.currentItem);
                }
            });

        document
            .querySelector(".carousel-indicators")
            .addEventListener("click", function (e) {
                let dots = document.querySelectorAll(".carousel-indicators li");
                var target = [].slice
                    .call(e.target.parentNode.children)
                    .indexOf(e.target);
                if (target !== window.currentItem && target < dots.length) {
                    ModDAO.goToItem(target);
                }
            });
    }
    // -------- .LIGHTBOX MEDIAS ------- //

    // -------- INCREASE LIKES ------- //
    //close modal
    const clickLike = document.querySelectorAll(".likes_media");
    clickLike.forEach((btn) =>
        btn.addEventListener("click", MedDAO.incrementLike)
    );
    // -------- .INCREASE LIKES ------- //

    addEventListener("keydown", (e) => {
        if (e.key === "Escape") {
            FormuDAO.closeModal();
            ModDAO.closeModalMedia();
        }

        if (e.key === "ArrowRight") {
            document.querySelector(".carousel-control.right").click();
        }

        if (e.key === "ArrowLeft") {
            document.querySelector(".carousel-control.left").click();
        }
    });
}, 100);
