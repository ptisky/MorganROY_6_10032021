class Callback {
    /**
     *
     * @returns JSON
     */
    async loadJson() {
        const getData = await fetch("./json/FishEyeDataFR.json");
        return (await getData).json();
    }
}
