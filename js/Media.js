class Media {
    /**
     *
     * @param {*} id
     * @param {*} photographerId
     * @param {*} image
     * @param {*} video
     * @param {*} tags
     * @param {*} likes
     * @param {*} date
     * @param {*} price
     * @param {*} span
     */
    constructor(
        id,
        photographerId,
        image,
        video,
        tags,
        likes,
        date,
        price,
        span
    ) {
        this.id = id;
        this.photographerId = photographerId;
        this.image = image;
        this.video = video;
        this.tags = tags;
        this.likes = likes;
        this.date = date;
        this.price = price;
        this.span = span;
    }

    set id(value) {
        this._id = value;
    }

    set photographerId(value) {
        this._photographerId = value;
    }

    set image(value) {
        this._image = value;
    }

    set video(value) {
        this._video = value;
    }

    set tags(value) {
        this._tags = value;
    }

    set likes(value) {
        this._likes = value;
    }

    set date(value) {
        this._date = value;
    }

    set price(value) {
        this._price = value;
    }

    set span(value) {
        this._span = value;
    }

    get id() {
        return this._id;
    }

    get photographerId() {
        return this._photographerId;
    }

    get image() {
        return this._image;
    }

    get video() {
        return this._video;
    }

    get tags() {
        return this._tags;
    }

    get likes() {
        return this._likes;
    }

    get date() {
        return this._date;
    }

    get price() {
        return this._price;
    }

    get span() {
        return this._span;
    }
}
