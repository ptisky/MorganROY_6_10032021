class PhotographerDAO extends Callback {
    constructor() {
        super();
        this.myPhotographers = this.myPhotographers.bind(this);
        this.appendPhotographers = this.appendPhotographers.bind(this);
        this.fillHomePage = this.fillHomePage.bind(this);
        this.clickobserver = this.clickobserver.bind(this);
    }

    /**
     *
     * @returns
     */
    myPhotographers() {
        let json = [];
        let photographers = [];

        document.addEventListener("DOMContentLoaded", async () => {
            try {
                json = await this.loadJson();
            } catch (e) {
                console.log("Error!");
                console.log(e);
            }

            for (var i = 0; i < json.photographers.length; i++) {
                let obj = json.photographers[i];
                let photographer = new Photographer(
                    obj["name"],
                    obj["id"],
                    obj["city"],
                    obj["country"],
                    obj["tagline"],
                    obj["price"],
                    obj["tags"],
                    obj["portrait"]
                );

                photographers.push(photographer);
            }
        });

        return photographers;
    }

    /**
     * Create Photographers profile
     * @param {*} photographers
     * @returns
     */
    appendPhotographers(photographers) {
        let appendTags = "";
        let sectionPhotographers = "";

        for (var g = 0; g < photographers.tags.length; g++) {
            appendTags += `<li id="#${photographers.tags[g]}" class="filter">#${photographers.tags[g]}</li>`;
        }

        sectionPhotographers = `<section id="${photographers.id}">
                                        <a href="#" class="load">
                                            <img src="img/Photographers/${photographers.portrait}" alt="portrait de ${photographers.name}" class="img_${photographers.name}" />
                                            <h2>${photographers.name}</h2>
                                        </a>
                                        <h3>${photographers.city} ${photographers.country}</h3>
                                        <p>${photographers.tagline}</p>
                                        <em>${photographers.price}€/jour</em>
                                        <ul class='list_filter'>${appendTags}</ul>
                                    </section>`;

        return sectionPhotographers;
        //store section
    }

    /**
     * Fill the home page
     * @param {*} photographers
     */
    fillHomePage(photographers) {
        let appendSection = "";

        setTimeout(() => {
            if (window.location.hash != "") {
                let filter_target = document.getElementById(
                    window.location.hash
                );
                filter_target.classList.add("active");
            }

            //get section informations
            for (var i = 0; i < photographers.length; i++) {
                var obj = photographers[i];

                for (var y = 0; y < obj.tags.length; y++) {
                    var tags = obj.tags[y];

                    if (window.location.hash != "") {
                        if ("#" + tags == window.location.hash) {
                            appendSection += this.appendPhotographers(obj);
                        }
                    }
                }

                if (window.location.hash == "") {
                    appendSection += this.appendPhotographers(obj);
                }
            }

            var p = window.location.pathname;
            if (p === "/" || p.match(/^\/?index/)) {
                //append elements in section
                document.getElementById(
                    "home_container"
                ).innerHTML = appendSection;
            }
        }, 100);
    }

    /**
     *
     * @param {*} photographers
     */
    clickobserver(photographers) {
        setTimeout(() => {
            var links = document.querySelectorAll(".load");
            for (var i = 0; i < links.length; i++) {
                links[i].onclick = function (links) {
                    let userClickOnThisId = links["path"][2].getAttribute("id");
                    for (var i = 0; i < photographers.length; i++) {
                        var obj = photographers[i];
                        if (obj.id == userClickOnThisId) {
                            window.location.href =
                                "./photographer.html?id=" + obj.id;
                            break;
                        }
                    }
                };

                //if click enter on profile
                links[i].addEventListener("keyup", function (event) {
                    if (event.keyCode === 13) {
                        let userClickOnThisId = event.path[1].id;
                        for (var i = 0; i < photographers.length; i++) {
                            var obj = photographers[i];
                            if (obj.id == userClickOnThisId) {
                                window.location.href =
                                    "./photographer.html?id=" + obj.id;
                                break;
                            }
                        }
                    }
                });
            }
        }, 100);
    }

    /**
     *
     * @param {*} btn
     */
    redirect_filter(btn) {
        let filter_value = btn.target.innerText;
        let filter_target = document.getElementById(filter_value);
        if (filter_target.classList[1] == "active") {
            window.location.replace("./");
        } else {
            window.location.replace("./" + filter_value);
            document.location.reload();
        }
    }
}
